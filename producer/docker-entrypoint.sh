#!/bin/bash

HELP=false
HOST=${BROKER_HOST}
PORT=${BROKER_PORT}
VHOST=${BROKER_VHOST}
EXCHANGE=${BROKER_EXCHANGE}
USER=${BROKER_USER}
PASSWORD=${BROKER_PASSWORD}

function usage() {
cat <<EOF
Usage: $0 [options] payload

--help            prints this help
-h|--host         message broker host (defaults to '${HOST}')
-p|--port         message broker port (defaults to '${PORT}')
-v|--vhost        message broker vhost (defaults to '${VHOST}')
-e|--exchange     message broker exchange (defaults to '${EXCHANGE}')
-u|--user         broker API user (defaults to '${USER}')
-w|--password     broker API password

payload           JSON {"build": "2016.20", "timestamp": "2012-07-08 11:14:15"}
EOF
exit 0
}

function get_data() {
cat <<EOF
{
  "properties": {},
  "routing_key": "api",
  "payload": '${PAYLOAD}',
  "payload_encoding": "string"
}
EOF
}

OPTS=`getopt -o h:p:e:q:u:w: --long help,host:,port:,exchange:,queue:,user:,password: -n 'parse-options' -- "$@"`

if [ $? != 0 ] ; then usage >&2 ; exit 1 ; fi

eval set -- "${OPTS}"

while true; do
  case "$1" in
    --help ) HELP=true; shift ;;
    -h | --host ) HOST=$2; shift 2;;
    -p | --port ) PORT=$2; shift 2;;
    -v | --vhost ) VHOST=$2; shift 2;;
    -e | --exchange ) EXCHANGE=$2; shift 2;;
    -u | --user ) USER=$2; shift 2;;
    -w | --password ) PASSWORD=$2; shift 2;;
    -- ) shift; break ;;
    * ) echo "Internal error!" ; exit 1 ;;
  esac
done

PAYLOAD="$@"

if [[ "$HELP" = true || -z "$PAYLOAD" ]] ; then
    usage
fi

if echo ${PAYLOAD} | jq -e . >/dev/null 2>&1 ; then
    echo "Sending data $(get_data) to http://$HOST:$PORT/api/exchanges/$VHOST/$EXCHANGE/publish" ;
    curl -u ${USER}:${PASSWORD} -XPOST -d"$(get_data)" http://${HOST}:${PORT}/api/exchanges/${VHOST}/${EXCHANGE}/publish
else
    echo "Invalid payload '$PAYLOAD'. Not valid JSON!" >&2 ; exit 1 ;
fi
