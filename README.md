#  DEVOPS test task

## Requirements
* Docker

## Spusteni
* v rootu projektu spustit `docker-compose up [-d]`

## Vytvoreni zpravy
* v rootu projektu spustit `docker-compose run --rm producer '{"build": "2016.20", "timestamp": "2012-07-08 11:14:15"}'`

## TODO
* Osetreni navratove hodnoty volani API brokeru v producer
* Supervisor pro workery
* Testy
