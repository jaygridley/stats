<?php

require_once __DIR__ . '/vendor/autoload.php';

use Kdyby\Console\DI\BootstrapHelper;
use Nette\Configurator;

$configurator = new Configurator;

if (BootstrapHelper::setupMode($configurator)) {
    // pass
} elseif (getenv('NETTE_DEBUG') === '0' || getenv('NETTE_DEBUG') === '1') {
    $configurator->setDebugMode((bool) getenv('NETTE_DEBUG'));
}

$configurator->enableDebugger(__DIR__ . '/log');
$configurator->setTempDirectory(__DIR__ . '/temp');
$configurator->addConfig(__DIR__ . '/src/config/config.neon');

return $configurator->createContainer();
