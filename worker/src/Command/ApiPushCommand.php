<?php

namespace Stats\Command;

use Kdyby\Console\InvalidArgumentException;
use Kdyby\Console\InvalidStateException;
use Nette\Utils\Json;
use Nette\Utils\JsonException;
use PhpAmqpLib\Connection\AMQPStreamConnection;
use PhpAmqpLib\Message\AMQPMessage;
use Stats\Api\Client\IApiClient;
use Stats\Api\Client\IApiClientFactory;
use Stats\Broker\Connection\IBrokerConnectionFactory;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * Pushes messages from broker to the API choosen by type.
 *
 * @author Martin Odstrcilik <martin.odstrcilik@gmail.com>
 */
class ApiPushCommand extends Command
{
    public const TYPE_LOG = 'log';
    public const TYPE_EVENTS = 'events';

    private const ARG_TYPE = 'type';

    private const TYPES = [
        self::TYPE_LOG,
        self::TYPE_EVENTS,
    ];

    private const OPT_BROKER_HOST = 'broker-host';
    private const OPT_BROKER_PORT = 'broker-port';
    private const OPT_BROKER_USER = 'broker-user';
    private const OPT_BROKER_PASSWORD = 'broker-password';
    private const OPT_BROKER_VHOST = 'broker-vhost';

    /** @var IBrokerConnectionFactory */
    private $brokerConnectionFactory;

    /** @var AMQPStreamConnection */
    private $brokerConnection;

    /** @var IApiClientFactory */
    private $apiClientFactory;

    /** @var IApiClient */
    private $apiClient;

    /**
     * @param IBrokerConnectionFactory $factory
     * @param IApiClientFactory $apiClientFactory
     */
    public function __construct(IBrokerConnectionFactory $factory, IApiClientFactory $apiClientFactory)
    {
        parent::__construct();

        $this->brokerConnectionFactory = $factory;
        $this->apiClientFactory = $apiClientFactory;
    }

    protected function configure(): void
    {
        $this->setName('api:push');
        $this->setDescription('Pushes messages from broker to the API');

        $this->addArgument(self::ARG_TYPE, InputArgument::REQUIRED, 'API type. Possible values are [' . implode('|', self::TYPES) . ']');

        $this->addOption(self::OPT_BROKER_HOST, null, InputOption::VALUE_REQUIRED, 'Broker host', getenv('BROKER_HOST'));
        $this->addOption(self::OPT_BROKER_PORT, null, InputOption::VALUE_REQUIRED, 'Broker port', getenv('BROKER_PORT'));
        $this->addOption(self::OPT_BROKER_USER, null, InputOption::VALUE_REQUIRED, 'Broker host', getenv('BROKER_USER'));
        $this->addOption(self::OPT_BROKER_PASSWORD, null, InputOption::VALUE_REQUIRED, 'Broker password', getenv('BROKER_PASSWORD'));
        $this->addOption(self::OPT_BROKER_VHOST, null, InputOption::VALUE_REQUIRED, 'Broker vhost', getenv('BROKER_VHOST'));
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     *
     * @return int
     */
    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $type = $this->getType($input);

        $connection = $this->getConnection($input);

        $channel = $connection->channel();
        $channel->queue_declare($type, false, true, false, false);

        $output->writeln("<info> [*] Waiting for <comment>$type</comment> messages. To exit press CTRL+C</info>");

        $channel->basic_consume($type, '', false, false, false, false, function(AMQPMessage $message) use ($type, $output) {
            $output->writeln("<info> [x] Received <comment>$message->body</comment></info>");

            if ($this->pushMessage($type, $message->body)) {
                $output->writeln('<info> [x] Pushed</info>');
                $message->delivery_info['channel']->basic_ack($message->delivery_info['delivery_tag']);
                $output->writeln('<info> [x] Acknowledged</info>');
            } else {
                $message->delivery_info['channel']->basic_reject($message->delivery_info['delivery_tag'], true);
                $output->writeln('<error> [x] Failed</error>');
            }
        });

        while (\count($channel->callbacks)) {
            $channel->wait();
        }

        $channel->close();
        $connection->close();

        return 0;
    }

    /**
     * @param InputInterface $input
     *
     * @return string
     * @throws \Symfony\Component\Console\Exception\InvalidArgumentException
     * @throws \Kdyby\Console\InvalidArgumentException
     */
    private function getType(InputInterface $input): string
    {
        $type = $input->getArgument(self::ARG_TYPE);

        if (false === \in_array($type, self::TYPES, true)) {
            throw new InvalidArgumentException("Invalid API type '$type'. Possible values are [" . implode('|', self::TYPES) . "]");
        }

        return $type;
    }

    /**
     * @param InputInterface $input
     *
     * @return AMQPStreamConnection
     */
    private function getConnection(InputInterface $input): AMQPStreamConnection
    {
        if (null === $this->brokerConnection) {
            $this->brokerConnection = $this->brokerConnectionFactory->create(
                (string) $input->getOption(self::OPT_BROKER_HOST),
                (string) $input->getOption(self::OPT_BROKER_PORT),
                (string) $input->getOption(self::OPT_BROKER_USER),
                (string) $input->getOption(self::OPT_BROKER_PASSWORD),
                (string) $input->getOption(self::OPT_BROKER_VHOST)
            );
        }

        return $this->brokerConnection;
    }

    /**
     * @param string $type
     * @param string $message
     *
     * @return bool
     * @throws \Kdyby\Console\InvalidStateException
     */
    private function pushMessage(string $type, string $message): bool
    {
        try {
            $payload = Json::decode($message);
        } catch (JsonException $e) {
            throw new InvalidStateException($e->getMessage(), $e->getCode(), $e->getPrevious());
        }

        if ($payload ?? null) {
            $client = $this->getApiClient($type);
            $success = $client->push($payload);
        }

        return $success ?? false;
    }

    /**
     * @param string $type
     *
     * @return IApiClient
     */
    private function getApiClient(string $type): IApiClient
    {
        if (null === $this->apiClient) {
            $this->apiClient = $this->apiClientFactory->create($type);
        }

        return $this->apiClient;
    }
}
