<?php

namespace Stats\Broker\Connection;

use PhpAmqpLib\Connection\AMQPStreamConnection;

/**
 * @author Martin Odstrcilik <martin.odstrcilik@gmail.com>
 */
class BrokerConnectionFactory implements IBrokerConnectionFactory
{
    /**
     * @inheritdoc
     */
    public function create($host, $port, $user, $password, $vhost): AMQPStreamConnection
    {
        return new AMQPStreamConnection(
            $host,
            $port,
            $user,
            $password,
            $vhost,
            false,
            'AMQPLAIN',
            null,
            'en_US',
            10,
            30,
            null,
            false,
            15
        );
    }
}
