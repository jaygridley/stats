<?php

namespace Stats\Broker\Connection;

use PhpAmqpLib\Connection\AMQPStreamConnection;

/**
 * @author Martin Odstrcilik <martin.odstrcilik@gmail.com>
 */
interface IBrokerConnectionFactory
{
    /**
     * @param string $host
     * @param string $port
     * @param string $user
     * @param string $password
     * @param string $vhost
     *
     * @return AMQPStreamConnection
     */
    public function create($host, $port, $user, $password, $vhost): AMQPStreamConnection;
}
