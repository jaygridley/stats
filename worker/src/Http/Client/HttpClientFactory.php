<?php

namespace Stats\Http\Client;

use GuzzleHttp\Client;
use GuzzleHttp\HandlerStack;

/**
 * @author Martin Odstrcilik <martin.odstrcilik@gmail.com>
 */
class HttpClientFactory
{
    /**
     * @return Client
     */
    public function create(): Client
    {
        $handler = HandlerStack::create(new MockHandler());

        return new Client([
            'handler' => $handler,
        ]);
    }
}
