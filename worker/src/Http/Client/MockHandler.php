<?php

namespace Stats\Http\Client;

use GuzzleHttp\Psr7\Response;

/**
 * @author Martin Odstrcilik <martin.odstrcilik@gmail.com>
 */
class MockHandler extends \GuzzleHttp\Handler\MockHandler
{
    public function __construct()
    {
        $queue = [
            new Response(200),
        ];

        $callback = function() {
            $responses = [200, 500];

            $this->append(new Response($responses[array_rand($responses)]));
        };

        parent::__construct($queue, $callback);
    }
}
