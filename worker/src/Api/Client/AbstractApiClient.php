<?php

namespace Stats\Api\Client;

use GuzzleHttp\Client;

/**
 * @author Martin Odstrcilik <martin.odstrcilik@gmail.com>
 */
abstract class AbstractApiClient implements IApiClient
{
    /** @var Client */
    protected $httpClient;

    /**
     * @param Client $httpClient
     */
    public function __construct(Client $httpClient)
    {
        $this->httpClient = $httpClient;
    }
}
