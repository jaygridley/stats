<?php

namespace Stats\Api\Client;

/**
 * @author Martin Odstrcilik <martin.odstrcilik@gmail.com>
 */
interface IApiClient
{
    /**
     * @param mixed $payload
     *
     * @return bool
     */
    public function push(object $payload): bool;
}
