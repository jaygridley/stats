<?php

namespace Stats\Api\Client;

/**
 * @author Martin Odstrcilik <martin.odstrcilik@gmail.com>
 */
interface IApiClientFactory
{
    /**
     * @param string $type
     *
     * @return IApiClient
     */
    public function create(string $type): IApiClient;
}
