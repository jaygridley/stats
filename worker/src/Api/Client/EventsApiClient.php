<?php

namespace Stats\Api\Client;

use GuzzleHttp\Exception\RequestException;
use Kdyby\Console\InvalidArgumentException;
use Nette\Utils\DateTime;
use Tracy\Debugger;

/**
 * @author Martin Odstrcilik <martin.odstrcilik@gmail.com>
 */
class EventsApiClient extends AbstractApiClient
{
    /**
     * @inheritdoc
     * @throws \Kdyby\Console\InvalidArgumentException
     */
    public function push(object $payload): bool
    {
        try {
            $response = $this->httpClient->post('/api/events/create', [
                'form_params' => $this->preparePayload($payload),
            ]);
        } catch (RequestException $e) {
            Debugger::log($e);
        }

        if ($response ?? null) {
            $success = 200 === $response->getStatusCode();
        }

        return $success ?? false;
    }

    /**
     * @param mixed $payload
     *
     * @return array
     * @throws \Kdyby\Console\InvalidArgumentException
     */
    private function preparePayload(object $payload): array
    {
        if (isset($payload->build, $payload->timestamp)) {
            return [
                'event' => [
                    'id' => $payload->build,
                    'type' => 2,
                    'timestamp' => DateTime::from($payload->timestamp)->getTimestamp(),
                ]
            ];
        }

        throw new InvalidArgumentException('Unsupported payload version');
    }
}
