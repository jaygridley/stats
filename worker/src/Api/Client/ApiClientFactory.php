<?php

namespace Stats\Api\Client;

use Kdyby\Console\InvalidArgumentException;
use Stats\Command\ApiPushCommand;
use Stats\Http\Client\HttpClientFactory;

/**
 * @author Martin Odstrcilik <martin.odstrcilik@gmail.com>
 */
class ApiClientFactory implements IApiClientFactory
{
    /** @var HttpClientFactory */
    private $httpClientFactory;

    /**
     * @param HttpClientFactory $httpClientFactory
     */
    public function __construct(HttpClientFactory $httpClientFactory)
    {
        $this->httpClientFactory = $httpClientFactory;
    }

    /**
     * @param string $type
     *
     * @return IApiClient
     * @throws \Kdyby\Console\InvalidArgumentException
     */
    public function create(string $type): IApiClient
    {
        switch ($type) {
            case ApiPushCommand::TYPE_LOG:
                return new LogApiClient($this->httpClientFactory->create());
            break;

            case ApiPushCommand::TYPE_EVENTS:
                return new EventsApiClient($this->httpClientFactory->create());
            break;

            default:
                throw new InvalidArgumentException("Unsupported API client type '$type'");
        }
    }
}
